package com.kpi.yuriy.labwork3.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.kpi.yuriy.labwork3.InterpolationLagrange;
import com.kpi.yuriy.labwork3.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Graph_errors extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private GraphView graph_err;
    private TextView textView_error;

    private FloatingActionButton fab_add;
    private FloatingActionButton fab_share;

    private InterpolationLagrange il;

    private int startX;
    private int endX;
    private int numbersofGraph;
    private double[] numX;
    private double[] numY;

    private String mParam1;
    private String mParam2;

    public Graph_errors() {

    }

    public static Graph_errors newInstance(String param1, String param2) {
        Graph_errors fragment = new Graph_errors();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_graph_errors, container, false);
        graph_err = (GraphView) v.findViewById(R.id.graph_err);
        textView_error = (TextView) v.findViewById(R.id.textView_eror);
        fab_add = (FloatingActionButton) getActivity().findViewById(R.id.fab_add);
        fab_share = (FloatingActionButton) getActivity().findViewById(R.id.fab_share1);
        il = new InterpolationLagrange();
        Bundle args = getArguments();
        if(args == null){
            textView_error.setText(R.string.there_is_nothing_to_build);
        }
        else{
            textView_error.setText(R.string.graph_errors);
            numX = args.getDoubleArray("arrayOfX");
            numY = args.getDoubleArray("arrayOfY");
            startX = args.getInt("min");
            endX = args.getInt("max");
            numbersofGraph = args.getInt("Numbers");
            fab_add.setVisibility(View.VISIBLE);
        }
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab_share.setVisibility(View.VISIBLE);
                DataPoint[] functionErr = new DataPoint[numbersofGraph];
                double h = (double) (endX - startX) / numbersofGraph;
                double fiy = 0;
                for (int i = 0; i < numbersofGraph; i++) {
                    fiy = il.Lagrange_Polynom_y(numX, numY, startX + i * h);
                    functionErr[i] = new DataPoint(startX + i * h, il.Lagrange_Errors(startX + i * h, fiy));
                }
                LineGraphSeries<DataPoint> ls1 = new LineGraphSeries<>(functionErr);
                ls1.setTitle("f(Error)");
                ls1.setColor(Color.BLACK);
                ls1.setThickness(8);
                graph_err.setVisibility(View.VISIBLE);
                graph_err.addSeries(ls1);
                graph_err.getViewport().setScalable(true);
                graph_err.getLegendRenderer().setVisible(true);
                graph_err.getLegendRenderer().setBackgroundColor(Color.argb(0x80, 0x80, 0x80, 0xff));
            }
        });
        fab_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fPath = Environment.getExternalStorageDirectory();
                File f = new File(fPath + "data/graph3.png");
                Bitmap returnedBitmap = il.getBipmapFromGraph(graph_err);
                try {
                    if (!f.exists()) {
                        f.createNewFile();
                    }
                    FileOutputStream os = new FileOutputStream(f);
                    returnedBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
                    os.close();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/png");
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()+"data/graph3.png");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share via"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return v;
    }

    @Override
    public void onDestroyView() {
        fab_add.setVisibility(View.GONE);
        fab_share.setVisibility(View.GONE);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
