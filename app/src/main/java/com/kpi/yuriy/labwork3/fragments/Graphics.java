package com.kpi.yuriy.labwork3.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.kpi.yuriy.labwork3.InterpolationLagrange;
import com.kpi.yuriy.labwork3.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class Graphics extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private GraphView graph_1;
    private GraphView graph_2;
    private TextView textView_gr;

    private FloatingActionButton fab_add;
    private FloatingActionButton fab_share1;
    private FloatingActionButton fab_share2;

    private InterpolationLagrange il;

    private int startX;
    private int endX;
    private int numbersofGraph;
    private double[] numX;
    private double[] numY;

    private String mParam1;
    private String mParam2;

    public Graphics() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Graphics.
     */
    // TODO: Rename and change types and number of parameters
    public static Graphics newInstance(String param1, String param2) {
        Graphics fragment = new Graphics();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_graphics, container, false);
        graph_1 = (GraphView) v.findViewById(R.id.graph_1);
        graph_2 = (GraphView) v.findViewById(R.id.graph_2);
        textView_gr = (TextView) v.findViewById(R.id.textView_gr);
        fab_add = (FloatingActionButton) getActivity().findViewById(R.id.fab_add);
        fab_share1 = (FloatingActionButton) getActivity().findViewById(R.id.fab_share1);
        fab_share2 = (FloatingActionButton) getActivity().findViewById(R.id.fab_share2);
        il = new InterpolationLagrange();
        Bundle args = getArguments();
        if(args == null){
            textView_gr.setText(R.string.there_is_nothing_to_build);
        }
        else{
            textView_gr.setText(R.string.graphs);
            numX = args.getDoubleArray("arrayOfX");
            numY = args.getDoubleArray("arrayOfY");
            startX = args.getInt("min");
            endX = args.getInt("max");
            numbersofGraph = args.getInt("Numbers");
            fab_add.setVisibility(View.VISIBLE);
        }
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab_share1.setVisibility(View.VISIBLE);
                fab_share2.setVisibility(View.VISIBLE);
                DataPoint[] functionS = new DataPoint[numbersofGraph];
                DataPoint[] interpolationS = new DataPoint[numbersofGraph];
                double h = (double)(endX - startX)/numbersofGraph;
                for (int i = 0; i < numbersofGraph; i++){
                    functionS[i] = new DataPoint(startX + i*h, function(startX + i*h));
                    interpolationS[i] = new DataPoint(startX + i*h, il.Lagrange_Polynom_y(numX, numY, startX + i*h));
                }

                LineGraphSeries<DataPoint> ls1 = new LineGraphSeries<>(functionS);
                ls1.setTitle("f(x)");
                ls1.setColor(Color.RED);
                ls1.setThickness(8);
                graph_1.setVisibility(View.VISIBLE);
                graph_1.addSeries(ls1);
                graph_1.getViewport().setScalable(true);
                graph_1.getLegendRenderer().setVisible(true);
                graph_1.getLegendRenderer().setBackgroundColor(Color.argb(0x80, 0x80, 0x80, 0xff));
                LineGraphSeries<DataPoint> ls2 = new LineGraphSeries<>(interpolationS);
                ls2.setColor(Color.BLUE);
                ls2.setThickness(8);
                ls2.setTitle("Intr(x)");
                graph_2.setVisibility(View.VISIBLE);
                graph_2.addSeries(ls2);
                graph_2.getViewport().setScalable(true);
                graph_2.getViewport().setScalableY(true);
                graph_2.getViewport().setScrollableY(true);
                graph_2.getLegendRenderer().setVisible(true);
                graph_2.getLegendRenderer().setBackgroundColor(Color.argb(0x80, 0x80, 0x80, 0xff));
            }
        });
        fab_share1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fPath = Environment.getExternalStorageDirectory();
                File f = new File(fPath + "data/graph1.png");
                Bitmap returnedBitmap = il.getBipmapFromGraph(graph_1);
                try {
                    if (!f.exists()) {
                        f.createNewFile();
                    }
                    FileOutputStream os = new FileOutputStream(f);
                    returnedBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
                    os.close();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/png");
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()+"data/graph1.png");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share via"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        fab_share2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fPath = Environment.getExternalStorageDirectory();
                File f = new File(fPath + "data/graph2.png");
                Bitmap returnedBitmap = il.getBipmapFromGraph(graph_2);
                try {
                    if (!f.exists()) {
                        f.createNewFile();
                    }
                    FileOutputStream os = new FileOutputStream(f);
                    returnedBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
                    os.close();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/png");
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()+"data/graph2.png");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share via"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return v;
    }

    public double function(double x){
        return Math.cos(x+Math.pow(Math.cos(x),3));
    }

    @Override
    public void onDestroyView() {
        fab_add.setVisibility(View.GONE);
        fab_share1.setVisibility(View.GONE);
        fab_share2.setVisibility(View.GONE);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
