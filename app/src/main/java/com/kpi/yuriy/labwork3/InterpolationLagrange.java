package com.kpi.yuriy.labwork3;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import com.jjoe64.graphview.GraphView;

public class InterpolationLagrange {


    public InterpolationLagrange(){

    }


    public double Lagrange_Polynom_y (double arrX[], double arrY[], double x){
        double multiply = 1;
        double sum = 0;

        for (int i = 0; i < arrX.length; i ++){
            multiply = 1;
            for (int j = 0; j < arrY.length; j++){
                if (j != i){
                    multiply *= (x - arrX[j]) / (arrX[i] - arrX[j]);
                }
            }
            sum += multiply * arrY[i];
        }

        return sum;
    }

    public double Lagrange_Errors (double x, double y){
        return Math.abs(function(x) - y);
    }

    public double function(double x){
        return Math.cos(x+Math.pow(Math.cos(x),3));
    }

    public Bitmap getBipmapFromGraph (GraphView gv){
        Bitmap vBitmap = Bitmap.createBitmap(gv.getWidth(), gv.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(vBitmap){
            @Override
            public boolean isHardwareAccelerated(){return true;}
        };
        Drawable drawable = gv.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        gv.draw(canvas);
        return vBitmap;
    }

}
