package com.kpi.yuriy.labwork3.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.kpi.yuriy.labwork3.R;


public class Main extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private CrystalSeekbar seekBar_X;
    private TextView textView_interpol;
    private Button button_X;

    private LinearLayout linear_eXs;
    private TextView textView_X1;
    private TextView textView_X2;

    private double progress;

    private OnFragmentInteractionListener mListener;

    public Main() {
        // Required empty public constructor
    }


    public static Main newInstance(String param1, String param2) {
        Main fragment = new Main();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        textView_interpol = (TextView) v.findViewById(R.id.textView_interpol);
        seekBar_X = (CrystalSeekbar) v.findViewById(R.id.seekBar_X);
        button_X = (Button) v.findViewById(R.id.button_X);
        linear_eXs = (LinearLayout) v.findViewById(R.id.linear_eXs);
        textView_X1 = (TextView) v.findViewById(R.id.textView_X1);
        textView_X2 = (TextView) v.findViewById(R.id.textView_X2);
        Bundle args = getArguments();
        if(args != null){
            textView_interpol.setVisibility(View.VISIBLE);
            seekBar_X.setVisibility(View.VISIBLE);
            button_X.setVisibility(View.VISIBLE);
            linear_eXs.setVisibility(View.VISIBLE);
            double[] buff = args.getDoubleArray("checkX");
            seekBar_X.setMinValue((float) buff[0]);
            seekBar_X.setMaxValue((float) buff[buff.length-1]);
            textView_X1.setText(String.valueOf(buff[0]));
            textView_X2.setText(String.valueOf(buff[buff.length-1]));
        }

        seekBar_X.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                progress = Double.parseDouble(String.valueOf(value));
                textView_X2.setText(String.valueOf(progress));
            }
        });

        button_X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentDouble(progress);
                Toast.makeText(getActivity(),"Number added to sms!", Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void onFragmentDouble(Double d);
    }
}
