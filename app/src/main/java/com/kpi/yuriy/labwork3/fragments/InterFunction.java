package com.kpi.yuriy.labwork3.fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.kpi.yuriy.labwork3.R;


public class InterFunction extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private EditText editText_number;
    private TextView textView_bounds;
    private CrystalRangeSeekbar range_bar;
    private EditText editText_numbers;
    private Button button_generate;
    private GridLayout table;

    private LinearLayout linear_numbers;
    private LinearLayout linear_range;
    private LinearLayout linear_graphic;

    private TextView textView_minBound;
    private TextView textView_maxBound;
    private TextView textView_1;
    private TextView textView_2;
    private LinearLayout[][] cells;

    private int minValueBar;
    private int maxValueBar;
    private double[] arrX;
    private double[] arrY;

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public InterFunction() {

    }

    public static InterFunction newInstance(String param1, String param2) {
        InterFunction fragment = new InterFunction();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_inter_function, container, false);

        editText_number = (EditText) v.findViewById(R.id.editText_number);
        editText_numbers = (EditText) v.findViewById(R.id.editText_numbers);
        textView_bounds = (TextView) v.findViewById(R.id.textView_bounds);
        range_bar = (CrystalRangeSeekbar) v.findViewById(R.id.range_bar);
        button_generate = (Button) v.findViewById(R.id.button_generate);
        table = (GridLayout) v.findViewById(R.id.table);
        linear_numbers = (LinearLayout) v.findViewById(R.id.linear_numbers);
        linear_range = (LinearLayout) v.findViewById(R.id.linear_range);
        linear_graphic = (LinearLayout) v.findViewById(R.id.linear_graphic);
        textView_minBound = (TextView) v.findViewById(R.id.textView_minBound);
        textView_maxBound = (TextView) v.findViewById(R.id.textView_maxBound);
        range_bar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minValueBar = Integer.parseInt(String.valueOf(minValue));
                maxValueBar = Integer.parseInt(String.valueOf(maxValue));
                textView_minBound.setText(String.valueOf(minValue));
                textView_maxBound.setText(String.valueOf(maxValue));
            }
        });

        button_generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText_number.getText().toString().isEmpty() || editText_numbers.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"One of the fields haven't entered!", Toast.LENGTH_SHORT).show();
                }
                else {
                    linear_numbers.setVisibility(View.GONE);
                    linear_range.setVisibility(View.GONE);
                    textView_bounds.setVisibility(View.GONE);
                    range_bar.setVisibility(View.GONE);
                    linear_graphic.setVisibility(View.GONE);
                    button_generate.setVisibility(View.GONE);
                    int N = Integer.parseInt(editText_number.getText().toString());
                    arrX = new double[N];
                    arrY = new double[N];
                    generateArray();
                    generateTable(N);
                    Toast.makeText(getActivity(), "Values with h and function generated", Toast.LENGTH_LONG).show();

                    int buff = Integer.parseInt(editText_numbers.getText().toString());
                    mListener.onXCalculation(arrX);
                    mListener.onXYCalculation(arrX, arrY, minValueBar, maxValueBar, buff);
                }
            }
        });

        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void generateTable (int N){
        table.removeAllViews();
        table.setColumnCount(2);
        table.setRowCount(N+1);
        cells = new LinearLayout[N+1][2];
        TextView tX = new TextView(getActivity());
        tX.setText("Value X");
        tX.setTextSize(18);
        tX.setTextColor(Color.BLACK);
        tX.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tX.setGravity(Gravity.CENTER);
        TextView tY = new TextView(getActivity());
        tY.setText("Value Y");
        tY.setTextSize(18);
        tY.setTextColor(Color.BLACK);
        tY.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tY.setGravity(Gravity.CENTER);
        cells[0][0] = new LinearLayout(getActivity());
        cells[0][1] = new LinearLayout(getActivity());
        cells[0][0].setLayoutParams(new LinearLayout.LayoutParams(600,60));
        cells[0][1].setLayoutParams(new LinearLayout.LayoutParams(600,60));
        cells[0][0].addView(tX);
        cells[0][1].addView(tY);
        table.addView(cells[0][0]);
        table.addView(cells[0][1]);
        for (int i = 1; i < N+1; i++){
            for (int j = 0; j < 2; j++){
                generateCell(i, j);
            }
        }
    }

    public void generateCell (int row, int col){
        cells[row][col] = new LinearLayout(getActivity());
        cells[row][col].setOrientation(LinearLayout.VERTICAL);
        TextView text = new TextView(getActivity());
        text.setText(String.valueOf(row-1));
        text.setLayoutParams(new LinearLayout.LayoutParams(600,100));
        text.setGravity(Gravity.CENTER);
        TextView textView = new TextView(getActivity());
        if(col == 0){
            textView.setText(String.valueOf(arrX[row-1]));
        }else{
            textView.setText(String.valueOf(arrY[row-1]));
        }
        textView.setTextColor(Color.BLACK);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(new LinearLayout.LayoutParams(600,100));
        cells[row][col].addView(text);
        cells[row][col].addView(textView);
        cells[row][col].setLayoutParams(new LinearLayout.LayoutParams(600, 200));
        table.addView(cells[row][col]);
    }

    public void generateArray(){
        String buff = editText_number.getText().toString();
        double h = (maxValueBar - minValueBar)/Double.parseDouble(buff);
        for (int i = 0; i < arrX.length; i++){
            arrX[i] = minValueBar + h*i;
            arrY[i] = function(arrX[i]);
        }

    }

    public double function(double x){
        return Math.cos(x+Math.pow(Math.cos(x),3));
    }

    public interface OnFragmentInteractionListener {
        void onXCalculation(double[] x);
        void onXYCalculation(double[] x, double[] y, int min, int max, int num);
    }
}
